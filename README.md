# 99probs
These are my solutions for the [99 problems](https://wiki.haskell.org/H-99:_Ninety-Nine_Haskell_Problems).

Note that many are rather haphazardly put together, and I'll likely do a clean version in the future.

However, I want to this as a baseline to compare myself to later.
