module Lib
    ( someFunc
    ) where
import GHC.Fingerprint (fingerprint0)
import Data.Bits (Bits(xor))

someFunc :: IO ()
someFunc = putStrLn "someFunc"

-- Problem 1
myLast :: [a] -> a
myLast [] = error "no last elem in empty list"
myLast [x] = x
myLast (_:xs) = myLast xs

-- Problem 2
myButLast :: [a] -> a
myButLast = head . tail . reverse 

-- Problem 3
elementAt x t = x !! (t-1)

-- Problem 4
myLength :: [a] -> Int
myLength [] = 0
myLength [x] = 1
myLength (x:xs) = 1 + myLength xs

-- Problem 5
myReverse :: [a] -> [a]
myReverse [] = []
myReverse [x] = [x]
myReverse (x:xs) = myReverse xs ++ [x]

-- Problem 6
isPalindrome :: (Eq a) => [a] -> Bool
isPalindrome [] = True
isPalindrome [x] = True
isPalindrome (x:xs) =  (x == last xs) && isPalindrome (init xs)

-- Problem 7
data NestedList a = Elem a | List [NestedList a]

flatten :: NestedList a -> [a]
flatten (Elem x) = [x]
flatten (List []) = []
flatten (List (x:xs)) = flatten x ++ flatten (List xs)

-- Problem 8
compress :: Eq a => [a] -> [a]
compress [] = []
compress (x:xs) = x : compress ( dropWhile (== x) xs)

-- Problem 9
pack :: Eq a => [a] -> [[a]]
pack [] = []
pack (x:xs) = (x : takeWhile (== x) xs) : pack (dropWhile (== x) xs)

-- Problem 10
encode :: Eq a => [a] -> [(Int, a)]
encode  = map (\xs -> (length xs, head xs)) . pack 

-- Problem 11
data Item a = Multiple Int a | Single a
  deriving (Show)

encodeModified :: Eq a => [a] -> [Item a]
encodeModified = map encodeHelper . encode
  where
    encodeHelper (1,x) = Single x
    encodeHelper (n,x) = Multiple n x

-- Problem 12
decodeModified :: Eq a => [Item a] -> [a]
decodeModified = concatMap decodeHelper
  where
    decodeHelper (Single x) = [x]
    decodeHelper (Multiple n x) = replicate n x

-- Problem 13
-- encode-direct :: Eq a => [a] -> [Item a]
-- TODO

-- Problem 14
dupli :: [a] -> [a]
dupli [] = []
dupli (x:xs) = x:x:dupli xs

-- Problem 15
repli :: [a] -> Int -> [a]
repli [] _ = []
repli _ 0 = []
repli (x:xs) n = replicate n x ++ repli xs n

-- Problem 16
dropEvery :: [a] -> Int -> [a]
dropEvery [] _ = []
dropEvery _ 1 = []
dropEvery xs n = map snd $ filter (\x -> fst x `mod` n /= 0) (indexed xs)
  where 
    indexed xs = helper xs 1
      where
        helper :: [a] -> Int -> [(Int, a)]
        helper [] _ = []
        helper (x:xs) n = (n, x) : helper xs (n+1)
